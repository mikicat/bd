select distinct e.num_dpt, d.nom_dpt, d.ciutat_dpt
from departaments d, empleats e
where e.num_dpt = d.num_dpt and not exists(select e1.num_empl
                 from empleats e1
                 where e1.num_proj != e.num_proj and e1.num_dpt = e.num_dpt)
order by num_dpt asc, nom_dpt asc, ciutat_dpt asc