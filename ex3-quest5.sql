/*    presentacioTFG(idEstudiant, titolTFG, dniDirector, dniPresident, dniVocal, instantPresentacio, nota) 


Hi ha una fila de la taula per cada treball final de grau (TFG) que estigui pendent de ser presentat o que ja s'hagi presentat.

En la creació de la taula cal que tingueu en compte que:
- L'identificador de l'estudiant i el títol del TFG són chars de 100 caràcters.
- Un estudiant només pot presentar un TFG.
- No hi pot haver dos TFG amb el mateix títol.
- Tot TFG ha de tenir un títol.
- El director, el president i el vocal han de ser professors que existeixin a la base de dades.
- El director del TFG no pot estar en el tribunal del TFG (no pot ser ni president, ni vocal).
- El president i el vocal no poden ser el mateix professor.
- L'instant de presentació ha de ser un enter diferent de nul.
- La nota ha de ser un enter entre 0 i 10.
- La nota té valor nul fins que s'ha fet la presentació del TFG.

Respecteu els noms i l'ordre en què apareixen les columnes (fins i tot dins la clau o claus que calgui definir). Tots els noms s'han de posar en majúscues/minúscules com surt a l'enunciat.
*/
create table presentacioTFG(
	idEstudiant char(100) primary key,
	titolTFG char(100) unique not null,
	dniDirector char(50) not null,
	dniPresident char(50) not null,
	dniVocal char(50) not null,
	instantPresentacio integer not null,
	nota integer default null check (nota is null or (nota >= 0 and nota <= 10)),
	foreign key(dniDirector) references professors(dni),
	foreign key(dniPresident) references professors(dni),
	foreign key(dniVocal) references professors(dni),
	check (dniDirector != dniPresident and dniDirector != dniVocal),
	check (dniPresident != dniVocal)
);
