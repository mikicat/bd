/* Doneu una sentència SQL per obtenir el nom dels empleats que guanyen el sou més alt. 
 * Cal ordenar el resultat descendenment per nom de l'empleat. 
*/
select distinct nom_empl from empleats where (sou = (select MAX(sou) from empleats)) order by nom_empl desc