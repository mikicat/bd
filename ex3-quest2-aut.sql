/*
Doneu una sentència SQL per incrementar en 500000 
el pressupost dels projectes que tenen algun empleat que treballa a BARCELONA.
Pel joc de proves que trobareu al fitxer adjunt, el pressupost del projecte 
que hi ha d'haver després de l'execució de la sentència és 1100000 
*/
update projectes
set pressupost = pressupost + 500000 
where (select count(*) from empleats e 
where e.num_proj = projectes.num_proj and (select d.ciutat_dpt from departaments d where e.num_dpt = d.num_dpt) = 'BARCELONA') > 0