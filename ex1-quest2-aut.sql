/*
Doneu una sentència SQL per obtenir els números i els noms dels projectes que tenen assignats dos o més empleats.
Cal ordenar el resultat descendement per número de projecte.
*/
select p.num_proj, p.nom_proj from projectes p 
where ( (select count(*) from empleats e where e.num_proj = p.num_proj) >= 2
) 
order by p.num_proj desc