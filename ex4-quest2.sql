create table VENDES(
NUM_VENDA integer primary key,
NUM_EMPL integer not null,
CLIENT char(30),
unique (NUM_EMPL, CLIENT),
foreign key (NUM_EMPL) references empleats(num_empl)
);
create table PRODUCTES_VENUTS(
NUM_VENDA integer,
PRODUCTE char(20),
QUANTITAT integer not null default 1,
IMPORT integer,
primary key (NUM_VENDA, PRODUCTE),
foreign key (NUM_VENDA) references VENDES(NUM_VENDA)
);