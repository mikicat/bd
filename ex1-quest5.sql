select d.modul, avg(a.instantfi - a.instantinici) as mitjana_durada from despatxos d, assignacions a
where (a.instantfi is not null and d.modul = a.modul) 
group by d.modul
order by d.modul asc