create function comprova() returns trigger as $$
declare missatge varchar(500);
	begin
		if exists(select * from dia d where d.dia = 'dijous') then 
			SELECT texte INTO missatge FROM missatgesExcepcions WHERE num=1;
			RAISE EXCEPTION '%',missatge;
		end if;
		return null;
	exception 
		when raise_exception then raise exception '%',sqlerrm;
	end;
$$ language plpgsql;

create trigger restrict_dia
before delete on empleats
for each statement execute procedure comprova();