/*
Select count(*) as quant
From assignacions ass
Where ass.instantInici>50
Group by ass.instantInici
order by quant;

El resultat haurà de ser:

quant
1
2
*/                                             

insert into despatxos values ('omega', '123', 20);
insert into despatxos values ('omega', '124', 19);
insert into professors values ('1234', 'harry', null, 5000);
insert into assignacions values ('1234', 'omega', '123', 60, 70);
insert into assignacions values ('1234', 'omega', '124', 80, null);
insert into assignacions values ('1234', 'omega', '123', 80, null);
