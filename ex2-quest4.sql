#A=empleats{nom_empl->nom_empl_1, ciutat_empl->ciutat_empl_1, num_empl->num_empl_1, sou->sou_1, num_dpt->num_dpt_1, num_proj->num_proj_1}
B=A[ciutat_empl_1<>ciutat_empl, num_dpt_1=num_dpt]empleats
R=B[num_dpt*num_dpt]departaments
S=R[num_dpt, nom_dpt]
/* Doneu una seqüència d'operacions de l'àlgebra relacional per obtenir el 
 * número i nom dels departaments que tenen dos o més empleats que viuen a ciutats diferents. */
