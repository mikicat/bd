create function comprova() returns trigger as $$
declare missatge varchar(500);
	begin
		if (old.nempl = 123) then
			SELECT texte INTO missatge FROM missatgesExcepcions WHERE num=1;
			RAISE EXCEPTION '%',missatge;
		end if;
		if (TG_OP = 'UPDATE') then 
			return new;
		else return old;
		end if;
		
	exception 
	when raise_exception then raise exception '%',sqlerrm;
	end;
$$ language plpgsql;

create trigger restrict_upd
before update of nempl on empleats
for each row execute procedure comprova();

create trigger restrict_del
before delete on empleats
for each row execute procedure comprova();

DELETE FROM empleats WHERE nempl=123; 