create function comprova() returns trigger as $$
    declare missatge varchar(100);
    begin
        if (new.ciutat1 not in (select e.ciutat2 from empleats2 e)) then
            SELECT texte INTO missatge FROM missatgesExcepcions WHERE num=1;
            RAISE EXCEPTION '%',missatge;
        else return new;
        end if;
    exception
        when raise_exception then raise exception '%', sqlerrm;
    end;
$$ language plpgsql;

create trigger assert before
insert or update of ciutat1 on empleats1
for each row execute procedure comprova();