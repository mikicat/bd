/*
Doneu una sentència SQL per obtenir el nom dels professors que 
o bé se sap el seu número de telèfon (valor diferent de null) 
i tenen un sou superior a 2500, o bé no se sap el seu número 
de telèfon (valor null) i no tenen cap assignació a un despatx 
amb superfície inferior a 20.
*/
select p.nomprof from professors p
where (p.telefon is not null and p.sou > 2500) or 
(p.telefon is null and not exists 
(select * from assignacions a, despatxos d where a.dni = p.dni and a.numero = d.numero and a.modul = d.modul 
and d.superficie < 20)
)