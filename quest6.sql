create type res as( dni char(8), nom char(30), sou float4, plus float4, matricula char(10));
create or replace function llistat_treb(dniIni char(8), dniFi char(8))
returns setof res as $$
declare r res;
	mat char(10);
	missatge varchar(50);

begin
	for r in select dni, nom, sou_base as sou, plus 
		from treballadors
		where dni between dniIni and dniFi
	loop 
		if ((select count(*) from lloguers_actius l where l.dni = r.dni) > 4) then 
			for mat in select l.matricula 
			from lloguers_actius l
			where l.dni = r.dni
			order by l.matricula asc
			loop 
				r.matricula = mat; 
				return next r;
			end loop;
		else return next r;
		end if;
		
	end loop;
		
	if not found then 
		SELECT texte INTO missatge FROM missatgesExcepcions WHERE num=1;     
		RAISE EXCEPTION '%',missatge;
	end if;
exception 
	when raise_exception then raise exception '%',sqlerrm;
	WHEN others THEN 
		SELECT texte INTO missatge FROM missatgesExcepcions WHERE num=2;     
		RAISE EXCEPTION '%',missatge;
	
end;$$language plpgsql;
SELECT * FROM llistat_treb('11111111','33333333');
