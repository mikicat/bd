#A = professors(sou > 2500)
B = despatxos[modul, numero]
C = despatxos(superficie < 20)
D = C[modul, numero]
E = assignacions*D
F = A[nomprof]
G = E[dni]
H = professors*G
I = professors - H
J = I[nomprof]
K = F_u_J
/* Doneu una seqüència d'operacions en àlgebra relacional per obtenir el 
 * nom dels professors que o bé tenen un sou superior a 2500, 
 * o bé no tenen cap assignació a un despatx amb superfície inferior a 20.*/
/*
    Joc de proves: JP6 => No funciona quan hi ha dos professors (amb sou inferior a 2500). Un té una assignació a un despatx amb superfície superior a 20, i l'altre una assignació a un despatx amb superfície inferior.
    Joc de proves: JP7 => No funciona quan hi ha un professor que cobra menys de 2500 i no té cap assignació.
*/