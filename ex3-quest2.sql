/* Doneu una sentència SQL per obtenir el número i 
 * nom dels departaments que tenen dos o més empleats 
 * que viuen a ciutats diferents. */

select d.num_dpt, d.nom_dpt 
from departaments d
where (select count(distinct e.ciutat_empl) from empleats e where e.num_dpt = d.num_dpt) >= 2


