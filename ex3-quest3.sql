/* Suposem la base de dades que podeu trobar al fitxer adjunt.
Suposem que aquesta base de dades està en un estat on no hi ha cap fila.
Doneu una seqüència de sentències SQL d'actualització (INSERTs i/o UPDATEs) 
que violi la integritat referencial de la clau forana de la taula Assignacions que referencia la taula Despatxos.
Les sentències NOMÉS han de violar aquesta restricció. 
*/
insert into despatxos values ('omega', '123', 20);
insert into professors values ('1234', 'harry', null, 5000);
insert into assignacions values ('1234', 'omega', '123', 1, null);
update assignacions set modul = 'a' where assignacions.dni = '1234'
